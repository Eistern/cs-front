import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {LoginDialogComponent} from './components/login-dialog/login-dialog.component';
import {RegisterDialogComponent} from './components/register-dialog/register-dialog.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent, LoginDialogComponent, RegisterDialogComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
