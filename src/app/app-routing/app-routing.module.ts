import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserInfoComponent } from '../components/user-info/user-info.component';
import {AdvertInfoComponent} from '../components/advert-info/advert-info.component';
import {AdvertSearchComponent} from '../components/advert-search/advert-search.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', component: AdvertSearchComponent},
  {path: 'info/:username', component: UserInfoComponent},
  {path: 'advert/:advertId', component: AdvertInfoComponent},
  {path: '**', redirectTo: ''}
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
