import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import { LoginDialogComponent } from './components/login-dialog/login-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from './app-routing/app-routing.module';
import { UserInfoComponent } from './components/user-info/user-info.component';
import {HttpClientModule} from '@angular/common/http';
import {MatCardModule} from '@angular/material/card';
import {MatDividerModule} from '@angular/material/divider';
import { AdvertInfoComponent } from './components/advert-info/advert-info.component';
import { SubscribersComponent } from './components/subscribers/subscribers.component';
import { MiniInfoComponent } from './components/subscribers/mini-info/mini-info.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import { AdvertDialogComponent } from './components/advert-dialog/advert-dialog.component';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import { AdvertSearchComponent } from './components/advert-search/advert-search.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ErrorPopupComponent } from './components/error-popup/error-popup.component';
import { RegisterDialogComponent } from './components/register-dialog/register-dialog.component';
import {CookieService} from 'ngx-cookie-service';
import { StarRatingComponent } from './components/star-rating/star-rating.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginDialogComponent,
    UserInfoComponent,
    AdvertInfoComponent,
    SubscribersComponent,
    MiniInfoComponent,
    AdvertDialogComponent,
    AdvertSearchComponent,
    ErrorPopupComponent,
    RegisterDialogComponent,
    StarRatingComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    MatCardModule,
    MatDividerModule,
    MatTooltipModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatTableModule,
    MatSnackBarModule
  ],
  providers: [CookieService],
  bootstrap: [AppComponent],
  entryComponents: [LoginDialogComponent]
})
export class AppModule { }
