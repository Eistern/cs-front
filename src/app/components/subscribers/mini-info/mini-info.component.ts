import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../../services/user-info.service';

@Component({
  selector: 'app-mini-info',
  templateUrl: './mini-info.component.html',
  styleUrls: ['./mini-info.component.css']
})
export class MiniInfoComponent implements OnInit {
  @Input('user') public user: User;

  constructor() { }

  ngOnInit(): void {
  }

}
