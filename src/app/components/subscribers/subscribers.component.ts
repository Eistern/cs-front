import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../services/user-info.service';

@Component({
  selector: 'app-subscribers',
  templateUrl: './subscribers.component.html',
})
export class SubscribersComponent implements OnInit {
  @Input() public users: Array<User>;

  constructor() { }

  ngOnInit(): void {
  }

}
