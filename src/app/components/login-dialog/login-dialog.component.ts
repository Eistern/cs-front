import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import {AuthService} from '../../services/auth.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
})
export class LoginDialogComponent implements OnInit {

  formGroup: FormGroup;
  hide = true;

  constructor(private formBuilder: FormBuilder,
              private dialogRef: MatDialogRef<LoginDialogComponent>,
              private authService: AuthService,
              private snackBar: MatSnackBar) {
    this.formGroup = formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit(): void {
  }

  save(): void {
    this.authService.authorize(this.formGroup.value).subscribe(
      () => {
          this.dialogRef.close();
        },
      () => {
        this.snackBar.open('Ошибка авторизации, пожалуйста, повторите ввод', 'ОК', {duration: 3000});
      }
    );
  }

  cancel(): void {
    this.dialogRef.close();
  }
}
