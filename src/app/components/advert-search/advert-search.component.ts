import {Component, OnDestroy, OnInit} from '@angular/core';
import {PageEvent} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {Advert, AdvertsService, AdvertType} from '../../services/adverts.service';
import {Subscription} from 'rxjs';
import {formatDate, registerLocaleData} from '@angular/common';
import localeRu from '@angular/common/locales/ru';
import localeRuExtra from '@angular/common/locales/extra/ru';
import {MatSnackBar} from '@angular/material/snack-bar';
import {HttpErrorResponse} from '@angular/common/http';
import {AuthService} from '../../services/auth.service';
registerLocaleData(localeRu, 'ru-RU', localeRuExtra);


@Component({
  selector: 'app-advert-search',
  templateUrl: './advert-search.component.html',
  styleUrls: ['./advert-search.component.css']
})
export class AdvertSearchComponent implements OnInit, OnDestroy {

  columns = [
    {key: 'name', header: 'Имя пользователя', cell: (row: Advert) => `@${row.owner.username}`},
    {key: 'publicDate', header: 'Дата публикации', cell: (row: Advert) => `${formatDate(row.publicationDateResolved, 'dd-MMM-yyyy', 'ru-RU')}`},
    {key: 'arrDate', header: 'Дата прибытия', cell: (row: Advert) => `${formatDate(row.arrivingDateResolved, 'dd-MMM-yyyy', 'ru-RU')}`},
    {key: 'deprtDate', header: 'Дата отправки', cell: (row: Advert) => `${formatDate(row.checkOutDateResolved, 'dd-MMM-yyyy', 'ru-RU')}`},
    {key: 'place', header: 'Адрес', cell: (row: Advert) => `${row.place.country}, ${row.place.city}, ${row.place.home}`}
  ];

  displayedColumns: string[];
  total: number;
  pageIndex: number;
  pageSize: number;
  dataSource: MatTableDataSource<Advert>;
  filterType: AdvertType;
  private subscription: Subscription;
  AdvertType = AdvertType;

  constructor(private advertsService: AdvertsService,
              private matSnackBar: MatSnackBar,
              private authService: AuthService) { }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource<Advert>();
    this.total = 0;

    this.pageSize = 10;
    this.pageIndex = 1;
    this.updateData();
    this.displayedColumns = this.columns.map(column => column.key);
    this.displayedColumns.push('actions');
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  paginatorChange(event: PageEvent): void {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.updateData();
  }

  click(): void {
    this.updateData();
  }

  private updateData(): void {
    this.subscription?.unsubscribe();
    this.subscription = this.advertsService.loadSearchResult(this.pageSize, this.pageIndex, this.filterType).subscribe(
      result => {
        result = result.map<Advert>(resultElement => this.advertsService.resolveAdvertDates(resultElement));
        this.dataSource.data = result;
        this.total = this.dataSource.data.length;
      }, (error: HttpErrorResponse) => {
        if (error == null || error.status === 401) {
          this.authService.authorized = false;
          this.authService.authToken = null;
        }
        this.matSnackBar.open(`Упс, ${error?.message}`, 'ОК', {duration: 3000});
      }
    );
  }
}
