import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertSearchComponent } from './advert-search.component';

describe('AdvertSearchComponent', () => {
  let component: AdvertSearchComponent;
  let fixture: ComponentFixture<AdvertSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
