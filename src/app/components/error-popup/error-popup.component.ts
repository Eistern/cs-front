import {Component, Inject, OnInit} from '@angular/core';
import {MAT_SNACK_BAR_DATA} from '@angular/material/snack-bar';
import {HttpResponse} from '@angular/common/http';

@Component({
  selector: 'app-error-popup',
  templateUrl: './error-popup.component.html',
})
export class ErrorPopupComponent implements OnInit {

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: HttpResponse<any>) {}

  ngOnInit(): void {
  }

}
