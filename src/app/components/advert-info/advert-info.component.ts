import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Advert, AdvertComment, AdvertComposite, AdvertsService, AdvertType} from '../../services/adverts.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {Observable, Subscription} from 'rxjs';

@Component({
  selector: 'app-advert-info',
  templateUrl: './advert-info.component.html',
})
export class AdvertInfoComponent implements OnInit, OnDestroy {
  public advert: Advert;
  public comments: Array<AdvertComment>;
  public formGroup: FormGroup;
  private subscription: Subscription;
  private id: number;

  AdvertType = AdvertType;

  constructor(
    private formBuilder: FormBuilder,
    private currentRoute: ActivatedRoute,
    private advertsService: AdvertsService,
    public authService: AuthService
  ) {
    this.formGroup = formBuilder.group({
      message: ['', Validators.compose([Validators.maxLength(256), Validators.required])]
    });
  }

  ngOnInit(): void {
    this.id = parseInt(this.currentRoute.snapshot.paramMap.get('advertId'), 10);
    const advertObservable = this.advertsService.loadById(this.id);
    this.handleNewAdvert(advertObservable);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  subscribe() {
    this.subscription?.unsubscribe();
    this.advertsService.subscribeUserTo(this.id).subscribe(
      result => {
        this.advert = result;
        this.advertsService.resolveAdvertDates(this.advert);
      }
    );
  }

  addComment() {
    this.handleNewAdvert(this.advertsService.addCommentTo(this.id, this.formGroup.value));
  }

  private handleNewAdvert(observable: Observable<AdvertComposite>) {
    this.subscription?.unsubscribe();
    this.subscription = observable.subscribe(
      result => {
        this.advert = result.advert;
        if (typeof result.advert.advertType === 'string') {
          this.advert.advertType = result.advert.advertType === 'HOUSE_SEARCH' ? AdvertType.HOUSE_SEARCH : AdvertType.HOUSE_PROVISION;
        }
        this.comments = result.comments;
        this.advertsService.resolveAdvertDates(this.advert);
      }
    );
  }
}
