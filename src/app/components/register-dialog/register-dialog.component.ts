import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';
import {AuthService} from '../../services/auth.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-register-dialog',
  templateUrl: './register-dialog.component.html',
})
export class RegisterDialogComponent implements OnInit {

  formGroup: FormGroup;
  hide = true;

  constructor(private formBuilder: FormBuilder,
              private dialogRef: MatDialogRef<RegisterDialogComponent>,
              private authService: AuthService,
              private snackBar: MatSnackBar) {
    this.formGroup = formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      age: ['', Validators.min(18)]
    });
  }

  ngOnInit(): void {
  }

  save(): void {
    this.authService.register(this.formGroup.value).subscribe(
      () => {
        this.dialogRef.close();
      },
      () => {
          this.snackBar.open('Ошибка регистрации, введенное имя занято', 'ОК', {duration: 3000});
      }
    );
  }

  cancel(): void {
    this.dialogRef.close();
  }

}
