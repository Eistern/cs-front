import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.css']
})
export class StarRatingComponent implements OnInit {

  @Input() currentRating: number;
  @Input() maxRating: number;
  @Input() id: string;

  @Output() action: EventEmitter<number> = new EventEmitter<number>();

  public buttonEnumeration: Array<number>;
  public buttonDefaultValueEnumeration: Array<boolean>;

  constructor() { }

  ngOnInit(): void {
    this.maxRating = Math.round(this.maxRating);
    this.buttonEnumeration = Array<number>(this.maxRating * 2).fill(0).map((x, i) => i + 1);
    this.buttonDefaultValueEnumeration = Array<boolean>(this.maxRating * 2).fill(false);

    if (this.currentRating !== null) {
      this.currentRating = Math.round(this.currentRating * 2) / 2;
      if (this.currentRating !== 0) {
        this.buttonDefaultValueEnumeration[this.maxRating * 2 - (this.currentRating * 2 - 1)] = true;
      }
    }
  }

  emit(buttonNumber: number): void {
    const result = this.maxRating - buttonNumber / 2 + 0.5;
    this.action.emit(result);
  }
}
