import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {User, UserInfoService} from '../../services/user-info.service';
import { ActivatedRoute } from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {DOCUMENT} from '@angular/common';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit, OnDestroy {
  public user: User;
  private subscription: Subscription;
  private document: Document;

  constructor(
    private currentRoute: ActivatedRoute,
    private userInfoService: UserInfoService,
    @Inject(DOCUMENT) document: Document
  ) {
    this.document = document;
  }

  processObservable(observe: Observable<User>): void {
    this.subscription?.unsubscribe();
    this.subscription = observe.subscribe(
      result => this.user = result
    );
  }

  ngOnInit(): void {
    const username = this.currentRoute.snapshot.paramMap.get('username');
    this.processObservable(this.userInfoService.load(username));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  consumeCsRatingChange(csRating: number): void {
    console.log('cs');
    console.log(csRating);
    this.processObservable(this.userInfoService.addCsRating(this.user.username, csRating));
  }

  consumeHcRatingChange(hcRating: number) {
    console.log('hc');
    console.log(hcRating);
    this.processObservable(this.userInfoService.addHcRating(this.user.username, hcRating));
  }
}
