import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertDialogComponent } from './advert-dialog.component';

describe('AdvertDialogComponent', () => {
  let component: AdvertDialogComponent;
  let fixture: ComponentFixture<AdvertDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
