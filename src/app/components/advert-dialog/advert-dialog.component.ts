import { Component, OnInit } from '@angular/core';
import {AdvertsService} from '../../services/adverts.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {MatDialogRef} from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import {ErrorPopupComponent} from '../error-popup/error-popup.component';

@Component({
  selector: 'app-advert-dialog',
  templateUrl: './advert-dialog.component.html',
})
export class AdvertDialogComponent implements OnInit {
  public formGroup: FormGroup;

  currentDate: Date;
  currentArrivalDate: Date;
  currentDeprtDate: Date;
  datesCorrect = false;

  get confirmButtonDisabled(): boolean {
    return !this.formGroup.valid || !this.datesCorrect;
  }

  constructor(
    private formBuilder: FormBuilder,
    private dialogControl: MatDialogRef<AdvertDialogComponent>,
    private advertsService: AdvertsService,
    private snackBar: MatSnackBar
  ) {
    this.formGroup = formBuilder.group({
      header: ['', Validators.required],
      message: ['', Validators.required],
      advertType: [null, Validators.required],
      peopleNumber: [1, Validators.min(1)],
      arrivingDateResolved: [null, Validators.required],
      checkOutDateResolved: [null, Validators.required],
      country: ['', Validators.required],
      city: ['', Validators.required],
      home: ['', Validators.required]
    });
    this.currentDate = new Date();
    this.currentArrivalDate = this.currentDate;
  }

  ngOnInit(): void {
  }

  changeArrDate(type: string, event: MatDatepickerInputEvent<Date>): void {
    this.currentArrivalDate = event.value;
    this.checkDates();
  }

  private checkDates(): void {
    this.datesCorrect =
      this.currentDeprtDate != null && this.currentArrivalDate != null
      && this.currentArrivalDate <= this.currentDeprtDate;
  }

  changeDeprtDate(type: string, event: MatDatepickerInputEvent<Date>): void {
    this.currentDeprtDate = event.value;
    this.checkDates();
  }

  cancel() {
    this.dialogControl.close();
  }

  submit() {
    const observable = this.advertsService.save(this.formGroup.value);
    observable.subscribe(() => {
      this.dialogControl.close();
    },
      error => {
        this.snackBar.openFromComponent(ErrorPopupComponent, {data: error, duration: 3000});
      }
    );
  }
}
