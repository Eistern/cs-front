import {Component, OnInit} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {LoginDialogComponent} from './components/login-dialog/login-dialog.component';
import {AuthService} from './services/auth.service';
import {AdvertDialogComponent} from './components/advert-dialog/advert-dialog.component';
import {RegisterDialogComponent} from './components/register-dialog/register-dialog.component';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'cs';
  tokenName = 'JWTToken';

  constructor(private dialog: MatDialog,
              private cookieService: CookieService,
              public authService: AuthService) {}

  ngOnInit(): void {
    if (this.cookieService.check(this.tokenName)) {
      this.authService.authorized = true;
      this.authService.authToken = this.cookieService.get(this.tokenName);
    } else {
      this.cookieService.delete(this.tokenName);
    }
  }

  loginButtonAction() {
    if (this.authService.authorized) {
      this.authService.deauthorize();
      return;
    }
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    this.dialog.open(LoginDialogComponent, dialogConfig);
  }

  addAdvertAction() {
    if (!this.authService.authorized) {
      return;
    }
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    this.dialog.open(AdvertDialogComponent, dialogConfig);
  }

  registerButtonAction() {
    if (this.authService.authorized) {
      return;
    }
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    this.dialog.open(RegisterDialogComponent, dialogConfig);
  }
}
