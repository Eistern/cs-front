import { AuthService } from './auth.service';
import {asyncData} from '../../test';

const organisedData = {
  age: 20,
  securedUser: {
    username: 'Bob',
    password: 'test'
  }
};
const plainData = {
  age: 20,
  username: 'Bob',
  password: 'test'
};

describe('AuthService', () => {
  let service: AuthService;
  let utilsServiceSpy: {postByUrl: jasmine.Spy};
  let cookieServiceSpy: {delete: jasmine.Spy};

  beforeEach(() => {
    utilsServiceSpy = jasmine.createSpyObj('UtilsService', ['postByUrl']);
    cookieServiceSpy = jasmine.createSpyObj('CookieService', ['delete']);
    service = new AuthService(utilsServiceSpy as any, cookieServiceSpy as any);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be able to deauthorize user', () => {
    cookieServiceSpy.delete.and.callFake(
      (name: string) => {
        expect(name).toEqual('JWTToken', 'while deauthorizing, call delete on JWTToken cookie');
      }
    );

    service.deauthorize();
    expect(service.authorized).toBeFalsy('user is not authorized after this method');
    expect(service.authToken).toBeNull('saved token must be deleted');
  });

  it('should be able to register user', () => {
    utilsServiceSpy.postByUrl.and.callFake(
      (url: string, credential: any) => {
        expect(url).toEqual('http://localhost:8083/auth/register', 'registration data should be sent to corresponding api url');
        expect(credential).toEqual(organisedData, 'should convert plain data to organised');
        return asyncData({});
      }
    );

    service.register(plainData).subscribe(
      () => {},
      fail
    );

    expect(utilsServiceSpy.postByUrl.calls.count()).toBe(1, 'one request to utilService per method');
  });

  it('should be able to authorize user', () => {
    utilsServiceSpy.postByUrl.and.callFake(
      (url: string) => {
        expect(url).toEqual('http://localhost:8083/auth/login', 'login data should be sent to corresponding api url');
        return asyncData({token: 'test'});
      }
    );

    service.authorize(plainData).subscribe(
      () => {},
      fail
    );

    expect(utilsServiceSpy.postByUrl.calls.count()).toBe(1, 'one request to utilService per method');
  });
});
