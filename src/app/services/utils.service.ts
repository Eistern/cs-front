import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {
  private options = {
    headers: new HttpHeaders({
      Accept: 'application/json'
    }),
    withCredentials: true
  };

  public handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.statusText}, Backend returned number ${error.status}, body was: ${error.error} `);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }

  private formOptionsWithParams(params: HttpParams): {headers: HttpHeaders, withCredentials: boolean, params: HttpParams} {
    return {
      headers: this.options.headers,
      withCredentials: this.options.withCredentials,
      params
    };
  }

  public getByUrl<T>(url: string): Observable<T> {
    return this.http.get<T>(url, this.options).pipe(
      catchError(this.handleError)
    );
  }

  public getByUrlWithParams<T>(url: string, params: HttpParams): Observable<T> {
    const optionsWithParams = this.formOptionsWithParams(params);
    return this.http.get<T>(url, optionsWithParams).pipe(
      catchError(this.handleError)
    );
  }

  public postByUrl<T>(url: string, bodyContent?: any): Observable<T> {
    return this.http.post<T>(url, bodyContent, {withCredentials: true}).pipe(
      catchError(this.handleError)
    );
  }

  public postByUrlWithParams<T>(url: string, bodyContent: any, params: HttpParams): Observable<T> {
    const optionsWithParams = this.formOptionsWithParams(params);
    return this.http.post<T>(url, bodyContent, optionsWithParams).pipe(
      catchError(this.handleError)
    );
  }

  public putByUrl<T>(url: string, bodyContent?: any): Observable<T> {
    return this.http.put<T>(url, bodyContent, this.options).pipe(
      catchError(this.handleError)
    );
  }

  public putByUrlWithParams<T>(url: string, bodyContent: any, params: HttpParams): Observable<T> {
    const optionsWithParams = this.formOptionsWithParams(params);
    return this.http.put<T>(url, bodyContent, optionsWithParams).pipe(
      catchError(this.handleError)
    );
  }

  constructor(private http: HttpClient) { }

}
