import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UtilsService } from './utils.service';
import {HttpParams} from '@angular/common/http';

const userUrl = (userId: string) => `http://localhost:8083/userinfo/${userId}`;
const securedUserUrl = (userId: string) => `http://localhost:8083/user/${userId}`;
const updateUserCsRatingUrl = (userId: string) => securedUserUrl(userId) + '/changeCsRate';
const updateUserHcRatingUrl = (userId: string) => securedUserUrl(userId) + '/changeHcRate';

export interface User {
  userid: number;
  username: string;
  age: number;
  couchSerferRating: number;
  couchSerferRatingsNum: number;
  houseProvisionRating: number;
  houseProvisionRatingsNum: number;
}

@Injectable({
  providedIn: 'root'
})
export class UserInfoService {

  constructor(private utils: UtilsService) { }

  load(username: string): Observable<User> {
    return this.utils.getByUrl<User>(userUrl(username));
  }

  addCsRating(username: string, rating: number): Observable<User> {
    let httpParams = new HttpParams();
    httpParams = httpParams.set('rate', String(rating));
    return this.utils.putByUrlWithParams<User>(updateUserCsRatingUrl(username), {}, httpParams);
  }

  addHcRating(username: string, rating: number): Observable<User> {
    let httpParams = new HttpParams();
    httpParams = httpParams.set('rate', String(rating));
    return this.utils.putByUrlWithParams<User>(updateUserHcRatingUrl(username), {}, httpParams);
  }
}
