import { UtilsService } from './utils.service';
import {HttpErrorResponse, HttpParams} from '@angular/common/http';
import {asyncData, asyncError} from '../../test';

const testOkMessage = {message: 'Test complete'};
const errorResponse = new HttpErrorResponse({
  error: 'test 404 error',
  status: 404, statusText: 'Not Found'
});
const testSendMessage = {message: 'Testing content'};
let testParams = new HttpParams();

export function baseCheck(url, message) {
  if (message === testSendMessage) {
    return asyncData(testOkMessage);
  } else {
    return asyncError(errorResponse);
  }
}

export function parametersPostCheck(url, message, options) {
  if (options.params.keys().length !== 0 && message === testSendMessage) {
    return asyncData(testOkMessage);
  } else {
    return asyncError(errorResponse);
  }
}

export function parametersGetCheck(url, options) {
  if (options.params.keys().length !== 0) {
    return asyncData(testOkMessage);
  } else {
    return asyncError(errorResponse);
  }
}

describe('UtilsService', () => {
  let service: UtilsService;
  let httpClientSpy: {get: jasmine.Spy, post: jasmine.Spy, put: jasmine.Spy};

  beforeAll(() => {
    testParams = testParams.set('param', 'value');
  });

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put']);
    service = new UtilsService(httpClientSpy as any);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should make base get request', () => {
    httpClientSpy.get.and.returnValue(asyncData(testOkMessage));

    service.getByUrl<any>('http://test.url').subscribe(
      resultMessage => expect(resultMessage).toEqual(testOkMessage, 'should return test message'),
      fail
    );
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call per request');
  });

  it('should make typed base get request', () => {
    httpClientSpy.get.and.returnValue(asyncData(testOkMessage));

    service.getByUrl<{notMessage: string}>('http://test.url').subscribe(
      resultMessage => expect(resultMessage.notMessage).toBeUndefined('should return only declared fields'),
      fail
    );
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call per request');
  });

  it('should make parameters get request', () => {
    httpClientSpy.get.and.callFake(
      parametersGetCheck
    );

    service.getByUrlWithParams<any>('http://test.url', testParams).subscribe(
      resultMessage => expect(resultMessage).toEqual(testOkMessage, 'should return test message'),
      fail
    );
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call per request');
  });

  it('should make base post request', () => {
    httpClientSpy.post.and.callFake(
      baseCheck
    );

    service.postByUrl<any>('http://test.url', testSendMessage).subscribe(
      resultMessage => expect(resultMessage).toEqual(testOkMessage, 'should return test message'),
      fail
    );
    expect(httpClientSpy.post.calls.count()).toBe(1, 'one call per request');
  });

  it('should make typed base post request', () => {
    httpClientSpy.post.and.callFake(
      baseCheck
    );

    service.postByUrl<{notMessage: string}>('http://test.url', testSendMessage).subscribe(
      resultMessage => expect(resultMessage.notMessage).toBeUndefined('should return only declared fields'),
      fail
    );
    expect(httpClientSpy.post.calls.count()).toBe(1, 'one call per request');
  });

  it('should make parameters post request', () => {
    httpClientSpy.post.and.callFake(
      parametersPostCheck
    );

    service.postByUrlWithParams<any>('http://test.url', testSendMessage, testParams).subscribe(
      resultMessage => expect(resultMessage).toEqual(testOkMessage, 'should return test message'),
      fail
    );
    expect(httpClientSpy.post.calls.count()).toBe(1, 'one call per request');
  });

  it('should make base put request', () => {
    httpClientSpy.put.and.callFake(
      baseCheck
    );

    service.putByUrl<any>('http://test.url', testSendMessage).subscribe(
      resultMessage => expect(resultMessage).toEqual(testOkMessage, 'should return test message'),
      fail
    );
    expect(httpClientSpy.put.calls.count()).toBe(1, 'one call per request');
  });

  it('should make typed base put request', () => {
    httpClientSpy.put.and.callFake(
      baseCheck
    );

    service.putByUrl<{notMessage: string}>('http://test.url', testSendMessage).subscribe(
      resultMessage => expect(resultMessage.notMessage).toBeUndefined('should return only declared fields'),
      fail
    );
    expect(httpClientSpy.put.calls.count()).toBe(1, 'one call per request');
  });

  it('should make parameters put request', () => {
    httpClientSpy.put.and.callFake(
      parametersPostCheck
    );

    service.putByUrlWithParams<any>('http://test.url', testSendMessage, testParams).subscribe(
      resultMessage => expect(resultMessage).toEqual(testOkMessage, 'should return test message'),
      fail
    );
    expect(httpClientSpy.put.calls.count()).toBe(1, 'one call per request');
  });

  it('should handle errors', () => {
    httpClientSpy.get.and.returnValue(asyncError(errorResponse));

    try {
      service.getByUrl<{ notMessage: string }>('http://test.url').subscribe(
        () => fail('Expected error, no return'),
        error => expect(error).toEqual('Something bad happened; please try again later.')
      );
    } catch (e) {
      expect(e).toBeTruthy();
    }

    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call per request');
  });
});
