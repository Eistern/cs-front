import {Advert, AdvertComment, AdvertComposite, AdvertsService, AdvertType} from './adverts.service';
import {asyncData} from '../../test';
import {HttpParams} from '@angular/common/http';

const resultAdvert: Advert = {
  adId: 1,
  header: 'Head',
  message: 'Message',
  advertType: AdvertType.HOUSE_PROVISION,
  arrivingDate: 1,
  arrivingDateResolved: null,
  publicationDate: 1,
  publicationDateResolved: null,
  checkOutDate: 1,
  checkOutDateResolved: null,
  place: {
    country: 'RU',
    city: 'NSK',
    home: '1'
  },
  peopleNumber: 1,
  owner: {
    userid: 1,
    username: 'Bob',
    age: 20,
    houseProvisionRating: 5,
    houseProvisionRatingsNum: 1,
    couchSerferRatingsNum: 1,
    couchSerferRating: 5
  },
  subscribers: []
};

const saveAdvert = {
  adId: 1,
  header: 'Head',
  message: 'Message',
  advertType: AdvertType.HOUSE_PROVISION,
  arrivingDate: 1,
  arrivingDateResolved: null,
  publicationDate: 1,
  publicationDateResolved: null,
  checkOutDate: 1,
  checkOutDateResolved: null,
  country: 'RU',
  city: 'NSK',
  home: '1',
  peopleNumber: 1,
  owner: {
    userid: 1,
    username: 'Bob',
    age: 20,
    houseProvisionRating: 5,
    houseProvisionRatingsNum: 1,
    couchSerferRatingsNum: 1,
    couchSerferRating: 5
  },
  subscribers: []
};

describe('AdvertsService', () => {
  let service: AdvertsService;
  let utilsServiceSpy: {getByUrl: jasmine.Spy, getByUrlWithParams: jasmine.Spy, putByUrl: jasmine.Spy, postByUrl: jasmine.Spy};

  beforeEach(() => {
    utilsServiceSpy = jasmine.createSpyObj('UtilsService', ['getByUrl', 'getByUrlWithParams', 'putByUrl', 'postByUrl']);
    service = new AdvertsService(utilsServiceSpy as any);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be able to get advert by advert id', () => {
    utilsServiceSpy.getByUrl.and.callFake(
      (url: string) => {
        expect(url.startsWith('http://localhost:8083/advert'))
          .toBeTruthy('request for advert should start with api url');
        return asyncData(resultAdvert);
      }
    );

    service.loadById(0).subscribe(
      result => expect(result).toBeTruthy( 'should get sent advert'),
      fail
    );

    expect(utilsServiceSpy.getByUrl.calls.count()).toBe(1, 'one request to utilService per method');
  });

  it('should be able to subscribe user', () => {
    utilsServiceSpy.putByUrl.and.callFake(
      (url: string) => {
        expect(url.startsWith('http://localhost:8083/adchange'))
          .toBeTruthy('request for subscription should start with corresponding api url');
        return asyncData(resultAdvert);
      }
    );

    service.subscribeUserTo(0).subscribe(
      result => expect(result).toEqual(resultAdvert, 'should get sent advert'),
      fail
    );

    expect(utilsServiceSpy.putByUrl.calls.count()).toBe(1, 'one request to utilService per method');
  });

  it('should be able to save new advert', () => {
    utilsServiceSpy.postByUrl.and.callFake(
      (url: string, body: Advert) => {
        expect(url).toEqual('http://localhost:8083/adchange/add', 'request for add should match with corresponding api url');
        expect(body.place).toEqual(resultAdvert.place, 'should convert plain js object to structured one');
        return asyncData({ok: true});
      }
    );

    service.save(saveAdvert).subscribe(
      result => expect(result.ok).toBeTruthy('should return ok status'),
      fail
    );

    expect(utilsServiceSpy.postByUrl.calls.count()).toBe(1, 'one request to utilService per method');
  });

  it('should be able to make search request to the api', () => {
    const resultAdverts = [resultAdvert];

    utilsServiceSpy.getByUrlWithParams.and.callFake(
      (url: string, params: HttpParams) => {
        expect(url).toEqual('http://localhost:8083/advert', 'request for add should match with corresponding api url');
        expect(params.get('limit')).toEqual('10', 'should return 1st function parameter as limit parameter to http request');
        expect(params.get('pos')).toEqual('10', 'should return pageSize * pageNumber as pos parameter to http request');
        expect(params.has('type')).toBeFalsy('should not have type parameter if not given');
        return asyncData(resultAdverts);
      }
    );

    service.loadSearchResult(10, 1).subscribe(
      result => expect(result).toEqual(resultAdverts, 'should return array of adverts'),
      fail
    );

    expect(utilsServiceSpy.getByUrlWithParams.calls.count()).toBe(1, 'one request to utilService per method');
  });

  it('should be able to make search request to the api', () => {
    const resultAdverts = [resultAdvert];

    utilsServiceSpy.getByUrlWithParams.and.callFake(
      (url: string, params: HttpParams) => {
        expect(url).toEqual('http://localhost:8083/advert', 'request for add should match with corresponding api url');
        expect(params.get('type')).toEqual('HOUSE_SEARCH', 'should have type parameter if given');
        return asyncData(resultAdverts);
      }
    );

    service.loadSearchResult(10, 1, AdvertType.HOUSE_SEARCH).subscribe(
      result => expect(result).toEqual(resultAdverts, 'should return array of adverts'),
      fail
    );

    expect(utilsServiceSpy.getByUrlWithParams.calls.count()).toBe(1, 'one request to utilService per method');
  });

  it('should be able to make request for comment add', () => {
    const testComment: AdvertComment = { message: 'test', author: resultAdvert.owner, commentId: 0 };
    const testResult: AdvertComposite = {advert: resultAdvert, comments: [testComment]};

    utilsServiceSpy.postByUrl.and.callFake(
      (url: string) => {
        expect(url.startsWith('http://localhost:8083/comments')).toBeTruthy('request for comment post should match api url');
        return asyncData(testResult);
      }
    );

    service.addCommentTo(resultAdvert.adId, testComment).subscribe(
      result => expect(result).toEqual(testResult, 'should return AdvertComposite'),
      fail
    );

    expect(utilsServiceSpy.postByUrl.calls.count()).toBe(1, 'one request to utilService per method');
  });
});
