import { Injectable } from '@angular/core';
import {UtilsService} from './utils.service';
import {Observable} from 'rxjs';
import {CookieService} from 'ngx-cookie-service';

const loginUrl = 'http://localhost:8083/auth/login';
const registerUrl = 'http://localhost:8083/auth/register';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private utilsService: UtilsService,
    private cookieService: CookieService
  ) { }

  static authToken: string;
  static authorized = false;

  register(registerDetails: any): Observable<any> {
    const registerDetailsStructured = {
      age: registerDetails.age,
      securedUser: {
        username: registerDetails.username,
        password: registerDetails.password
      }
    };
    return this.utilsService.postByUrl(registerUrl, registerDetailsStructured);
  }

  deauthorize(): void {
    AuthService.authorized = false;
    AuthService.authToken = null;
    this.cookieService.delete('JWTToken');
  }

  authorize(loginDetails: any): Observable<{ token: string }> {
    const observable = this.utilsService.postByUrl<{ token: string }>(loginUrl, loginDetails);
    observable.subscribe(
      () => this.authorized = true
    );
    return observable;
  }

  get authorized() {
    return AuthService.authorized;
  }

  set authorized(value: boolean) {
    AuthService.authorized = value;
  }

  get authToken() {
    return AuthService.authToken;
  }

  set authToken(value: string) {
    AuthService.authToken = value;
  }
}
