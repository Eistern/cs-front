import { Injectable } from '@angular/core';
import { UtilsService } from './utils.service';
import { User } from './user-info.service';
import { Observable } from 'rxjs';
import {HttpParams, HttpResponse} from '@angular/common/http';

const allAdvertsUrl = 'http://localhost:8083/advert';
const singleAdvertUrl = (advertId: number) => allAdvertsUrl + `/${advertId}`;
const saveAdvertUrl = 'http://localhost:8083/adchange/add';
const subscribeAdvertUrl = (advertId: number) => `http://localhost:8083/adchange/${advertId}/addsubscriber`;
const commentAdvertUrl = (advertId: number) => `http://localhost:8083/comments/${advertId}/add`;

export enum AdvertType {
  HOUSE_SEARCH, HOUSE_PROVISION
}

export interface AdvertComposite {
  advert: Advert;
  comments: Array<AdvertComment>;
}

export interface AdvertComment {
  commentId: number;
  message: string;
  author: User;
}

export interface Place {
  country: string;
  city: string;
  home: string;
}

export interface Advert {
  adId: number;
  owner: User;
  publicationDate: number;
  subscribers: Array<User>;
  header: string;
  message: string;
  advertType: AdvertType;
  place: Place;
  peopleNumber: number;
  arrivingDate: number;
  checkOutDate: number;
  checkOutDateResolved: Date;
  arrivingDateResolved: Date;
  publicationDateResolved: Date;
}

@Injectable({
  providedIn: 'root'
})
export class AdvertsService {

  constructor(private utilsService: UtilsService) {}

  resolveAdvertDates(advert: Advert): Advert {
    advert.arrivingDateResolved = new Date(advert.arrivingDate);
    advert.checkOutDateResolved = new Date(advert.checkOutDate);
    advert.publicationDateResolved = new Date(advert.publicationDate);
    return advert;
  }

  reverseAdvertDates(advert: Advert): void {
    advert.arrivingDate = +advert.arrivingDateResolved;
    advert.checkOutDate = +advert.checkOutDateResolved;
    advert.publicationDate = +advert.publicationDateResolved;
  }

  loadById(adId: number): Observable<AdvertComposite> {
    return this.utilsService.getByUrl<AdvertComposite>(singleAdvertUrl(adId));
  }

  loadSearchResult(pageSize: number, pageNumber: number, type?: AdvertType): Observable<Array<Advert>> {
    let params = new HttpParams();
    params = params.set('limit', String(pageSize));
    params = params.set('pos', String(pageSize * pageNumber));
    if (type != null) {
      params = params.set('type', type === AdvertType.HOUSE_SEARCH ? 'HOUSE_SEARCH' : 'HOUSE_PROVISION');
    }
    return this.utilsService.getByUrlWithParams<Array<Advert>>(allAdvertsUrl, params);
  }

  save(advert: any): Observable<HttpResponse<any>> {
    this.reverseAdvertDates(advert);
    advert.place = {city: advert.city, country: advert.country, home: advert.home};
    delete advert.city;
    delete advert.country;
    delete advert.home;
    delete advert.arrivingDateResolved;
    delete advert.checkOutDateResolved;
    return this.utilsService.postByUrl<HttpResponse<any>>(saveAdvertUrl, advert);
  }

  subscribeUserTo(advertId: number): Observable<Advert> {
    return this.utilsService.putByUrl<Advert>(subscribeAdvertUrl(advertId));
  }

  addCommentTo(advertId: number, comment: {message: string}): Observable<AdvertComposite> {
    return this.utilsService.postByUrl<AdvertComposite>(commentAdvertUrl(advertId), comment);
  }
}
