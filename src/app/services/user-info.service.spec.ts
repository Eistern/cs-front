import {User, UserInfoService} from './user-info.service';
import {UtilsService} from './utils.service';
import {asyncData} from '../../test';
import {HttpParams} from '@angular/common/http';

const testUsername = 'Bob';
const user: User = {
  userid: 1,
  username: 'Bob',
  age: 20,
  couchSerferRating: 20,
  couchSerferRatingsNum: 5,
  houseProvisionRatingsNum: 1,
  houseProvisionRating: 4
};

describe('UserInfoService', () => {
  let service: UserInfoService;
  let utilsServiceSpy: {getByUrl: jasmine.Spy, putByUrlWithParams: jasmine.Spy};


  beforeEach(() => {
    utilsServiceSpy = jasmine.createSpyObj('UtilsService', ['getByUrl', 'putByUrlWithParams']);
    service = new UserInfoService(utilsServiceSpy as any);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should load user by it\'s username', () => {
    utilsServiceSpy.getByUrl.and.callFake(
      (url: string) => {
        expect(url.startsWith('http://localhost:8083/userinfo')).toBeTruthy('request url should start with common user api url');
        return asyncData(user);
      }
    );

    service.load(testUsername).subscribe(
      result => expect(result).toEqual(user, 'should receive sent information'),
      fail
    );

    expect(utilsServiceSpy.getByUrl.calls.count()).toBe(1, 'one request to utilService per method');
  });

  it('should be able to change user cs rate', () => {
    utilsServiceSpy.putByUrlWithParams.and.callFake(
      (url: string, content, params: HttpParams) => {
        expect(url.startsWith('http://localhost:8083/user')).toBeTruthy('request should start with secured user api url');
        expect(url.endsWith('changeCsRate')).toBeTruthy('request should specify that cs rate is changing');
        expect(params.has('rate')).toBeTruthy('request should include rate parameter');
        return asyncData(user);
      }
    );

    service.addCsRating(testUsername, 5).subscribe(
      result => expect(result).toEqual(user, 'should receive sent information'),
      fail
    );

    expect(utilsServiceSpy.putByUrlWithParams.calls.count()).toBe(1, 'one request to utilService per method');
  });

  it('should be able to change user hc rate', () => {
    utilsServiceSpy.putByUrlWithParams.and.callFake(
      (url: string, content, params: HttpParams) => {
        expect(url.startsWith('http://localhost:8083/user')).toBeTruthy('request should start with secured user api url');
        expect(url.endsWith('changeHcRate')).toBeTruthy('request should specify that hc rate is changing');
        expect(params.has('rate')).toBeTruthy('request should include rate parameter');
        return asyncData(user);
      }
    );

    service.addHcRating(testUsername, 5).subscribe(
      result => expect(result).toEqual(user, 'should receive sent information'),
      fail
    );

    expect(utilsServiceSpy.putByUrlWithParams.calls.count()).toBe(1, 'one request to utilService per method');
  });
});
